//Zongle Zhao
//CSE2-210
//HW9
//this homework gives you practice with arrays and in searching single dimensional arrays

import java.util.Scanner;
import java.util.Random;
public class RemoveElements{
  public static void main(String [] arg){
 Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
 String answer="";
 do{
   System.out.print("Random input 10 ints [0-9]");
   num = randomInput();
   String out = "The original array is:";
   out += listArray(num);
   System.out.println(out);
 
   System.out.print("Enter the index ");
   index = scan.nextInt();
   newArray1 = delete(num,index);
   String out1="The output array is ";
   out1+=listArray(newArray1); 
   System.out.println(out1);
 
      System.out.print("Enter the target value ");
   target = scan.nextInt();
   newArray2 = remove(num,target);
   String out2="The output array is ";
   out2+=listArray(newArray2); 
   System.out.println(out2);
    
   System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
   answer=scan.next();
 }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
 String out="{";
 for(int j=0;j<num.length;j++){
   if(j>0){
     out+=", ";
   }
   out+=num[j];
 }
 out+="} ";
 return out;
  }
  
  public static int[] randomInput(){
    Random rand = new Random();
    int array[] = new int [10];//list arrays
 for(int i = 0; i<array.length;i++){
      array[i] = rand.nextInt(10);
    }
    for (int i = 0; i < 10; i++){ //generate arrays
        int num = rand.nextInt(array.length);
        int j = array[0];
        array[0]=array[num];
        array[num] = j;
               
        
    }
     return array;
     
     
  }
  
  public static int[] delete(int[] list, int pos){
    Scanner scan = new Scanner (System.in);
   int array[] = new int[9];//creating new array
    
    String junk = "";
    
    int determinant = 0;
    
    do{
//check if it is within the range
    if(pos>=0 &&pos<=9  ){
      determinant = 1;
    }
    else{
      System.out.println("it is not within the range");
      pos = scan.nextInt();
    }
    }while(determinant != 1);
    
    
    for(int i = 0; i < pos; i++){//copy from old arrays
      array[i] = list[i];
      
      
    }
 for(int j = pos; j < array.length; j++){//copy from old arrays
      array[j] = list[j+1];
    }
    return array;
  }
  public static int[] remove(int[] list, int target){
    int count = 0;
    int j = 0;
    for(int i = 0; i < list.length; i++){//how many numbers it has
      if(list[i] != target){
        count++;
      
      
      }
    }
    int array[] = new int[count];//using new arrays
    
 for(int i = 0; i < list.length; i++){//copy from the old array
   if(list[i] != target){
     
        int answer = list[i];
        
        array[j] = answer;
        j++;
      }
    }
    return array;
  }
    
}

