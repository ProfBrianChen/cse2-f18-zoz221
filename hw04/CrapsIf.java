//Zongle Zhao
//9.24.2018
//section 210hw 04
//the objective of hw 04 is to practice with random number generation, selection statementy and string manipulations.

import java.util.Scanner;


public class CrapsIf{
  public static void main(String[] args){
    Scanner Archer = new Scanner(System. in);
    System.out.println("choose how you want to play this game: 1:randomly cast dice; 2:state the dice they want to evaluate ");
    int choice = Archer. nextInt();
    
    if(choice == 1){//if player wants to choose random number
      int DiceNum1 = (int) (Math.random()*6+1);//generate random numbers
    int DiceNum2 = (int) (Math.random()*6+1);
      System.out.println("Dice number 1 is "+ DiceNum1);
      System.out.println("Dice number 2 is "+ DiceNum2);
     if(DiceNum1 == 1){//state that when dice number 1 is equal to 1, how different dice number 2 would print out different results
       if(DiceNum2 == 1){
         System.out.println("Snake Eyes");
       }
       if(DiceNum2 == 2){
         System.out.println("Ace Deuce");
      }
       if(DiceNum2 == 3){
         System.out.println("Easy Four");
       }
       if(DiceNum2 == 4){
         System.out.println("Fever Five");
       }
       if(DiceNum2 == 5){
         System.out.println("Easy Six");
       }
       if(DiceNum2 == 6){
         System.out.println("Seven out");
       }
     }
      else if (DiceNum1 == 2){//state that when dice number 1 is equal to 2, how different dice number 2 would print out different results
        if(DiceNum2 == 1){
         System.out.println("Ace Deuce");
       }
       if(DiceNum2 == 2){
         System.out.println("Hard Four");
      }
       if(DiceNum2 == 3){
         System.out.println("Easy Four");
       }
       if(DiceNum2 == 4){
         System.out.println("Fever Five");
       }
       if(DiceNum2 == 5){
         System.out.println("Easy Six");
       }
       if(DiceNum2 == 6){
         System.out.println("Seven out");
       }
      }
      else if (DiceNum1 == 3){//state that when dice number 1 is equal to 3, how different dice number 2 would print out different results
         if(DiceNum2 == 1){
         System.out.println("Easy Four");
       }
       if(DiceNum2 == 2){
         System.out.println("Fever Five");
      }
       if(DiceNum2 == 3){
         System.out.println("Hard Six");
       }
       if(DiceNum2 == 4){
         System.out.println("Seven Out");
       }
       if(DiceNum2 == 5){
         System.out.println("Easy Eight");
       }
       if(DiceNum2 == 6){
         System.out.println("Nine");
       }
      }
      else if(DiceNum1 == 4){//state that when dice number 1 is equal to 4, how different dice number 2 would print out different results
        if(DiceNum2 == 1){
         System.out.println("Fever Five");
       }
       if(DiceNum2 == 2){
         System.out.println("Easy Six");
      }
       if(DiceNum2 == 3){
         System.out.println("Seven Out");
       }
       if(DiceNum2 == 4){
         System.out.println("Hard Eight");
       }
       if(DiceNum2 == 5){
         System.out.println("Nine");
       }
       if(DiceNum2 == 6){
         System.out.println("Easy Ten");
       }
      }
      else if(DiceNum1 ==5){//state that when dice number 1 is equal to 5, how different dice number 2 would print out different results
        if(DiceNum2 ==1){
         System.out.println("Easy Six");
       }
       if(DiceNum2 == 2){
         System.out.println("Seven Out");
      }
       if(DiceNum2 == 3){
         System.out.println("Easy Eight");
       }
       if(DiceNum2 == 4){
         System.out.println("Nine");
       }
       if(DiceNum2 == 5){
         System.out.println("Hard ten");
       }
       if(DiceNum2 == 6){
         System.out.println("Yo-leven");
       }
      }
      else{//state that when dice number 1 is equal to 6, how different dice number 2 would print out different results
        if(DiceNum2 == 1){
         System.out.println("Seven Out");
       }
       if(DiceNum2 == 2){
         System.out.println("Easy Eight");
      }
       if(DiceNum2 == 3){
         System.out.println("Nine");
       }
       if(DiceNum2 == 4){
         System.out.println("Easy Ten");
       }
       if(DiceNum2 == 5){
         System.out.println("Yo-leven");
       }
       if(DiceNum2 == 6){
         System.out.println("Boxcars");
       }
      }
      }
    
    else if(choice == 2){//if players want to choose number by themselves
      Scanner scn = new Scanner(System. in);
      System.out.println("dice number 1 is: ");
      int ChooseNum1 = scn. nextInt();
      
      System.out.println("dice number 2 is: ");
      int ChooseNum2 = scn. nextInt();
      
      if(ChooseNum1 == 1){//state that when dice number 1 is equal to 1, how different dice number 2 would print out different results
       if(ChooseNum2 == 1){
         System.out.println("Snake Eyes");
       }
       if(ChooseNum2 == 2){
         System.out.println("Ace Deuce");
      }
       if(ChooseNum2 == 3){
         System.out.println("Easy Four");
       }
       if(ChooseNum2 ==4){
         System.out.println("Fever Five");
       }
       if(ChooseNum2 == 5){
         System.out.println("Easy Six");
       }
       if(ChooseNum2 == 6){
         System.out.println("Seven out");
       }
        else{//state that when dice number 1 is not within the range
        System.out.println("not within the range of numbers");
      }
     }
      else if (ChooseNum1 == 2){//state that when dice number 1 is equal to 2, how different dice number 2 would print out different results
        if(ChooseNum2 == 1){
         System.out.println("Ace Deuce");
       }
       if(ChooseNum2 == 2){
         System.out.println("Hard Four");
      }
       if(ChooseNum2 == 3){
         System.out.println("Easy Four");
       }
       if(ChooseNum2 == 4){
         System.out.println("Fever Five");
       }
       if(ChooseNum2 == 5){
         System.out.println("Easy Six");
       }
       if(ChooseNum2 == 6){
         System.out.println("Seven out");
       }
        else{//state that when dice number 1 is not within the range
        System.out.println("not within the range of numbers");
      }
      }
      else if (ChooseNum1 == 3){//state that when dice number 1 is equal to 3, how different dice number 2 would print out different results
         if(ChooseNum2 == 1){
         System.out.println("Easy Four");
       }
       if(ChooseNum2 == 2){
         System.out.println("Fever Five");
      }
       if(ChooseNum2 == 3){
         System.out.println("Hard Six");
       }
       if(ChooseNum2 == 4){
         System.out.println("Seven Out");
       }
       if(ChooseNum2 == 5){
         System.out.println("Easy Eight");
       }
       if(ChooseNum2 == 6){
         System.out.println("Nine");
       }
        else{//state that when dice number 1 is not within the range
        System.out.println("not within the range of numbers");
      }
      }
      else if(ChooseNum1 == 4){//state that when dice number 1 is equal to 4, how different dice number 2 would print out different results
        if(ChooseNum2 == 1){
         System.out.println("Fever Five");
       }
       if(ChooseNum2 == 2){
         System.out.println("Easy Six");
      }
       if(ChooseNum2 == 3){
         System.out.println("Seven Out");
       }
       if(ChooseNum2 == 4){
         System.out.println("Hard Eight");
       }
       if(ChooseNum2 == 5){
         System.out.println("Nine");
       }
       if(ChooseNum2== 6){
         System.out.println("Easy Ten");
       }
        else{//state that when dice number 1 is not within the range
        System.out.println("not within the range of numbers");
      }
      }
      else if(ChooseNum1 == 5){//state that when dice number 1 is equal to 5, how different dice number 2 would print out different results
        if(ChooseNum2 == 1){
         System.out.println("Easy Six");
       }
       if(ChooseNum2 == 2){
         System.out.println("Seven Out");
      }
       if(ChooseNum2 == 3){
         System.out.println("Easy Eight");
       }
       if(ChooseNum2 == 4){
         System.out.println("Nine");
       }
       if(ChooseNum2 == 5){
         System.out.println("Hard ten");
       }
       if(ChooseNum2 == 6){
         System.out.println("Yo-leven");
       }
        else{//state that when dice number 1 is not within the range
        System.out.println("not within the range of numbers");
      }
      }
      else if (ChooseNum1 == 6){//state that when dice number 1 is equal to 6, how different dice number 2 would print out different results
        if(ChooseNum2 == 1){
         System.out.println("Seven Out");
       }
       if(ChooseNum2 == 2){
         System.out.println("Easy Eight");
      }
       if(ChooseNum2 == 3){
         System.out.println("Nine");
       }
       if(ChooseNum2 == 4){
         System.out.println("Easy Ten");
       }
       if(ChooseNum2 == 5){
         System.out.println("Yo-leven");
       }
       if(ChooseNum2 == 6){
         System.out.println("Boxcars");
       }
        else{//state that when dice number 1 is not within the range
        System.out.println("not within the range of numbers");
      }
      }
      else{//state that when dice number 1 is not within the range
        System.out.println("not within the range of numbers");
      }
      }
    else{
      System.out.println("not a choice");
    
    }
    }
    
    
      }
      
      
    
    
    
    
   
  


