//Zongle Zhao
//lab02  Cyclometer
//9/5/2018
//section 210
//bicycle cyclometer is used to measure the speed and distance. record two kinds of data, the time elapsed in seconds, and the number of rotations of the front wheel.

public class Cyclometer{
  public static void main(String args[]){
     int secsTrip1=480;//time required for the first trip
     int secsTrip2=3220;//time required for the second trip
     int countsTrip1=1561;//number of counts for the first trip
     int countsTrip2=9037;// number of counts for the second trip
     
    double wheelDiameter=27.0,//value of wheelDiameter
     pi=3.14159,//VALUE OF PI
     feetPerMile=5280,//feet per mile
     inchesPerFoot=12,//inches per foot
     secondsPerMinute=60;//seconds per minute
     double distanceTrip1,distanceTrip2,totalDistance;//enter variables of distance
    
    System.out.println("Trip one took "+ (secsTrip1/secondsPerMinute)+" minute and had "+countsTrip1+" counts.");
    System.out.println("Trip two took "+ (secsTrip2/secondsPerMinute)+" minute and had "+countsTrip2+" counts.");
    
    distanceTrip1=countsTrip1*pi*wheelDiameter;//calculate the distance travelled for the first trip in inchesPerFoot
    distanceTrip1/=inchesPerFoot*feetPerMile;//convert distance into miles
    
    distanceTrip2=countsTrip2*pi*wheelDiameter/inchesPerFoot/feetPerMile;//distance travelled for the first trip in miles
    totalDistance=distanceTrip2+distanceTrip1;//calculate the total distance.
    
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("Total distance was "+totalDistance+" miles");
                
    
    
    
    
  }
}