// Zongle Zhao
//lab03 Check
//9/12/2018
//section 210
//how to get input from the user and use that data to perform basic computations.

import java.util.Scanner;

public class Check{
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );//
    System.out.print("Enter the original cost of the check in the form xx.xx: ");//enter the cost amount of money
    double checkCost=myScanner.nextDouble();//accpet the user input
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx) : ");//enter the tip 
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; //convert percentage into a decimal value
     
    System.out.print("Enter the number of people who went out to dinner: ");//enter the number of people
    int numPeople = myScanner.nextInt();
    
    double totalCost;//input of total cost
    double costPerPerson;//input of cost per person
    int dollars, dimes, pennies; //whole dollar amount of cost, for storing digits.int
    totalCost = checkCost*(1+tipPercent);
    costPerPerson = totalCost/numPeople;//get rhe whole amount, dropping decimal fraction
    dollars = (int)costPerPerson;//calculation of dollars per person
    dimes = (int)(costPerPerson*10)%10;// calculation of dimes per person
    pennies = (int)(costPerPerson*100)%10;// calculation of pennies per person
    System.out.println("Each person in the group owes $" + dollars + '.' +dimes +pennies); 
  }
}