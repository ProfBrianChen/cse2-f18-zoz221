//Zongle Zhao
//9.9.2018
//section 210hw 02
//the objective of hw 02 is to give students practice manipulating data stored in variables. 

public class Arithmetic{
  public static void main(String args[]){
    int numPants=3;//Number of pairs of pants
    double pantsPrice=34.98;//cost per pair of pants
    
    int numShirts=2;//Number of sweatshirts
    double shirtPrice=24.99;//cost per shirt
    
    int numBelts=1;//number of belts
    double beltCost=33.99;//cost per belt
    
    double paSalesTax=0.06;//the tax rate
    
   
    
    
    
    
   double totalCostPants=numPants*pantsPrice;//calculating the total cost of pants.
    double totalCostShirts=numShirts*shirtPrice;//calculating the total cost of shirts.
    double totalCostBelts=numBelts*beltCost;//calculating the total cost of pants.
    
    double taxPants=totalCostPants*0.06;//tax of pants
    double taxShirts=totalCostShirts*0.06;//tax of shirts
    double taxBelts=totalCostBelts*0.06;//tax of belts
    
    double totalCost=totalCostBelts+totalCostShirts+totalCostPants;//total cost before tax
    double totalTax=taxBelts+taxShirts+taxPants;//total tax 
    
    double totalPaid=totalCost+totalTax;//total paid in transaction
    

    System.out.printf("the cost of pants is %.2f dollars", totalCostPants);//cost of pants
    System.out.println("");
    System.out.printf("the cost of shirts is %.2f dollars", totalCostShirts);//cost of shirts
    System.out.println("");
    System.out.printf("the cost of belts is %.2f dollars", totalCostBelts);//cost of belts
    System.out.println("");
    System.out.printf("the total cost is %.2f dollars", totalCost);//total cost
    System.out.println("");
    System.out.printf("the tax of pants is %.2f dollars", taxPants);//tax of pants
    System.out.println("");
    System.out.printf("the tax of shirts is %.2f dollars", taxShirts);//tax of shirts
    System.out.println("");
    System.out.printf("the tax of belts is %.2f dollars", taxBelts);//tax of belts
    System.out.println("");
    System.out.printf("the total tax is %.2f dollars", totalTax);//total tax
    System.out.println("");
    System.out.printf("the total paid is %.2f dollars", totalPaid);//total money paid
    System.out.println("");
    
  }
}