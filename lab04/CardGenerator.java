//Zongle(Archer) Zhao
//lab04 Card Generator
//9/19/2018
//section 210
//The objective is to use if statements, switch statements and in using Math. random()
// a random number generator.

public class CardGenerator{
  public static void main(String args[]){
    int cardNum = (int) (Math.random()*52+1);// random pick cards from 1 to 52
    int num = cardNum%13;// use modulus to calculate the number
    String suit;//string suit
    String identity;//string identity
    
    if(cardNum>=1 && cardNum <= 13){
      suit = "diamonds";//enter range of card number into different suit.
      
    }
      else if(cardNum >13 && cardNum <= 26){
        suit = "clubs";//enter range of card number into different suit
        
      }
      else if(cardNum >26 && cardNum<=39){
        suit = "hearts";//enter range of card number into different suit
      }
    else {
      suit = "spades";//enter range of card number into different suit
    }
    switch(num){//using switch to identify the identity of card
      case 1:
        identity = "Ace";
        break;
      case 2:
        identity = "2";
        break;
      case 3:
        identity = "3";
        break;
      case 4:
        identity = "4";
        break;
        case 5:
        identity = "5";
        break;
      case 6:
        identity = "6";
        break;
      case 7:
        identity = "7";
        break;
      case 8:
        identity = "8";
        break;
        case 9:
        identity = "9";
        break;
      case 10:
        identity = "10";
        break;
      case 11:
        identity = "Jack";
        break;
      case 12:
        identity = "Queen";
        break;
      case 0:
        identity = "King";
        break;
      default:
        identity = "invalid";
        break;
        
    }
    System.out.println("You picked the  "+ identity + "  of  " + suit );//print out the results.
  }
}
