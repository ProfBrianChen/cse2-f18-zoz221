//Zongle Zhao
//9.17.2018
//section 210hw 03
//the objective of hw 03 is to give students practice performing arithmetic operations. 

import java.util.Scanner;

public class Convert{
  public static void main(String[] args){
    Scanner John = new Scanner(System. in );
    System.out.print("Enter the affected area in acres: ");//enter the area affected in acres
    double affectArea = John. nextDouble();//accept the input
    
    System.out.print("Enter the rainfall in the affected area(inches):  ");//enter the rainfall in inches
    double rainfall= John.nextDouble();
    
    double gallons =  affectArea * Math.pow(10,6) * 6.27 * rainfall * 0.004329;//first convert the acres to inches and find out the volume and then convert the inches to gallons
    double cubicMiles = gallons * 9.08 * Math.pow(10,-13);//convert the gallons to cubic miles
    System.out.printf("%.8f\n", cubicMiles);//print out the results.
  }
}