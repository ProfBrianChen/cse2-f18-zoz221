//Zongle Zhao
//9.17.2018
//section 210hw 03
//the objective of hw 03 is to give students practice performing arithmetic operations. 

import java.util.Scanner;

public class Pyramid{
  public static void main(String[] args){
    Scanner John = new Scanner(System. in );
    System.out.print("The square side of the pyramid is (input length): ");//input of the square side length
    double length = John. nextDouble();
    
    System.out.print("The height of the pyramid is (input height):  ");//input the height
    double height= John.nextDouble();
    
    double volume = length * length * height / 3;//volume is equal to length square times the height times 1/3.
    System.out.println("The volume inside the pyramid is:" + volume+"." ); //print out the results
  }
}