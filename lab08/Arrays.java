//Zongle Zhao
//CSE-002-210
//Lab 08
//the objective of the lab is to let us be familiar with one dimensional arrays.

public class Arrays{
  public static void main(String[] args){
    int[] array1 = new int[100];//enter the first array
  int[] array2 = new int[100];//enter the second array
  int i =0;
  int count = 0;//initisalize count
  for(i=0;i<array1.length;i++){//generate 100 different numbers
   array1[i] = (int)(Math.random()*(100));
   
  }
  for(i=0; i<array1.length;i++){//using the second array to check counts
  count = array1 [i];
  array2[count]++;
  }
  
  for(i=0;i<array1.length;i++){//print out the counts
    if(array2[i]>=1){//show that the number has appeared at least once
    System.out.println("number "+ i+" occurs "+ array2[i]+" times ");
    }
  }
 
  
  }

}