//Zongle Zhao
//CSE002-210
//10/29/2018
//HW07
//Objective: the objective is to practice writing method, manipulating Strings and forcing them to enter good inputs.

import java.util.Scanner;//import the scanner

public class WordTools{
  
  public static void main(String args[]){//main method
    
    String textEnter = "";
  String sampleResults = SampleText();//using the SampleText method to store the sampletext
    System.out.println("Enter a sample text: " );
    System.out.println(sampleResults);//print out the sampleText
  String optionResults = printMenu();//using the printMenu method to operate different menu choices
  System.out.println(optionResults);
  
  if(optionResults.equals("c")){//different choices made by user would operate different command, thus use different methods
    System.out.println("number of non-space characters are:  "+getNumOfNonWSCharacters(sampleResults));
  }
  else if(optionResults.equals("w")){
  System.out.println("number of words: "+getNumOfWords(sampleResults));
  }
  else if(optionResults.equals("f")){
  System.out.println(" number of text: "+findText(textEnter,sampleResults));
  }
  else if(optionResults.equals("r")){
  System.out.println("edited text: "+replaceExclamation(sampleResults));
  
  }
  else if(optionResults.equals("s")){
  System.out.println("edited text: "+shortenSpace(sampleResults));
  }
  }
  
  public static String SampleText(){//ask the user for the text they want to use
  String sampleText = "";
  Scanner scn = new Scanner(System.in);
    System.out.println("enter the text:");
  sampleText = scn.nextLine();

  return sampleText;
  
  
  }
  
  public static String printMenu(){//let user to choose different options
  Scanner scn = new Scanner(System.in);
  System.out.println("Menu:");
  System.out.println("c-Number of non-whitespace characters");
  System.out.println("w-number of words");
  System.out.println("f-find text");
  System.out.println("Replace all !'s");
  System.out.println("Shorten spaces");
  System.out.println("Quit");
  System.out.println("Choose an option:");
  
  int determinant = 0;
  String option = "";
  String junk = "";
  do{//make sure the options are valid choice
    while(!scn.hasNextLine()){
      System.out.println("invalid input");
      junk = scn.nextLine();
    }
    option = scn.nextLine();
    if(option.equals("c") || option.equals("w") || option.equals("f") || option.equals("r") || option.equals("s")||option.equals("q")){
    determinant=1;
    }
    else{
    System.out.println("invalid input");
    }
    
    
    
  }while(determinant !=1);
  
 
  
  return option;
  }
  
  
  public static int getNumOfNonWSCharacters(String Cha){//count number of characters
    int count = 0;
    int length = Cha.length();//length of characters including spaces
    for(int i =0; i<length;i++){
      if(Cha.charAt(i)!=' '){
      count++;//make sure white spaces does not count
      }
    
    }
   
    
    return count;
  
  
  }
  
  public static int getNumOfWords(String str){//count number of words used
  

        int count = 0;
        for(int e = 0; e < str.length(); e++){//loop used make sure it is word that been counted
            if(str.charAt(e) != ' '){
                count++;
                while(str.charAt(e) != ' ' && e < str.length()-1){
                    e++;
                }
            }
        }
        return count;
    }
  
  
  
  public static int findText(String text1,String text2){//find text the user want
  Scanner scn = new Scanner(System.in);
  System.out.println("enter the text you want to find: ");
  text1 = scn.nextLine();
  int count =text2.split(text1).length;//count how many times the text appears
  
  return count;
  
  
  }
  
  public static String replaceExclamation(String str){
  String replaceString = str.replace('!','.');//replace methods
  
  return replaceString;
  }
  
  public static String shortenSpace(String str){
  String replaceString = str.replaceAll("\\s+"," ");//replace again
  
  return replaceString;
  
  }
  
  }
  
  
    
    
  
  
  
